# Transfuse

A script to backup (copy+compress) and restore your plasma desktop configurations.

Also includes options to export and use package lists for pacman and the arch user repository.

### Download and Use

##### Download
```text
curl -O https://gitlab.com/cscs/transfuse/-/raw/master/transfuse.sh
```

##### Mark Executable
```text
chmod +x transfuse.sh
```

##### Example usage:
```text
./transfuse.sh -b `whoami`
```

> Note: transfuse.sh will now present you with a menu if no argument is used

##### Help

```text
   TRANSFUSE                                                          
                                                                      
   A Script to Backup and Restore KDE Plasma Desktop Configurations   
                                                                      
   Usage: transfuse.sh [option] [USER/PATIENT]                        
                                                                      
   Options:                                                           
    -h   --help                                      show this help   
    -b   --backup USER               copy and compress USER configs   
    -bt  --backupt USER                 backup USER appearance only   
    -BR  --backuproot                           backup root configs   
    -C   --copy USER                       copy but do not compress   
    -c   --compress                         compress copied configs   
    -p   --pkglists               create list of installed packages * 
    -pr  --pkgrestore                        install native pkglist * 
    -pra --pkgrestorealien                    install alien pkglist * 
    -r   --restore PATIENT             restore configs /to/ PATIENT   
                                                                      
                                 NOTES                                
   Environment Variable        COVERED=1       Skip wallpaper steps   
   Environment Variable        CHARTS=1         More verbose output   
   (*) pkg* options require pacman package manager or an AUR helper   
```

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)